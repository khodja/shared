import HttpClient from '../utils/httpClient';

export const getProducts = (query = {}) => {
  return HttpClient.doGet('/products', query);
};

export const productById = (id: number, token: any) => {
  return HttpClient.doGet(`/product/` + id, {}, token);
};
export const productByBrandId = (id: number) => {
  return HttpClient.doGet(`/products/brand/` + id);
};

export const productsByCategory = (
  query = { id: 1, keyword: null, categories: null, brands: null },
) => {
  const { id, keyword, categories, brands } = query;
  return HttpClient.doGet(`/products/category/${id}`, {
    keyword,
    categories,
    brands,
  });
};

export const searchProduct = (query: any) => {
  const { keyword, categories, brands, order_by } = query;
  const data = {
    ...(!!keyword && { keyword }),
    ...(!!categories && { categories: JSON.stringify(categories) }),
    ...(!!brands && { brands: JSON.stringify(brands) }),
    ...(!!order_by && { order_by }),
  };
  return HttpClient.doGet(`/search`, data);
};
