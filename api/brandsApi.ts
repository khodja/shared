import HttpClient from "../utils/httpClient";

export const getBrands = (query = {}) => {
  return HttpClient.doGet("/brands", query);
};

export const getProductsBrands = (id: string | number) => {
  return HttpClient.doGet(`/products/brand/` + id);
};
