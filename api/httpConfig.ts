export const HttpConfig: any = {
  API_VERSION: 'v1/',
  STORE_VERSION: '1.0.0',
  API_PATH: 'https://admin.vippi.uz/api',
  BASE_URL: 'https://admin.vippi.uz/api',
};
// dev url 'http://188.166.233.211/api'
