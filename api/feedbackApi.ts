import HttpClient from '../utils/httpClient';

export const getFeedback = (query: any, token = null) => {
  return HttpClient.doGet(
    '/feedback/product/' + query.id,
    query,
    token,
  );
};

export const addFeedback = (query: any, token = null) => {
  const {
    product_rating,
    shipping_rating,
    product_feedback,
    shipping_feedback,
  } = query;
  const body = {
    product_rating,
    shipping_rating,
    product_feedback,
    shipping_feedback,
  };
  return HttpClient.doPost(
    '/feedback/order/' + query.id,
    body,
    token,
  );
};
export const addFeedbackProduct = (query: any, token = null) => {
  const { rating, comment, item_id } = query;
  const body = {
    rating,
    comment,
    item_id,
  };
  return HttpClient.doPost(
    '/add/feedback/product/' + query.id,
    body,
    token,
  );
};
