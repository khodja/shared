import HttpClient from '../utils/httpClient';

export const getNews = (query: any) => {
  return HttpClient.doGet('/news', query);
};

export const getNewsById = (id: string) => {
  return HttpClient.doGet('/news/' + id);
};
