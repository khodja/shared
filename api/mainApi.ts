import HttpClient from "../utils/httpClient";

export const getMain = (query: any) => {
  return HttpClient.doGet("/main", query, null);
};
