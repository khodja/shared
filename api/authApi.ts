import HttpClient from '../utils/httpClient';

export const authUser = (query: {} | undefined) => {
  return HttpClient.doPost('/auth', query);
};

export const verifyUser = (query: {} | undefined) => {
  return HttpClient.doPost('/verification', query);
};

export const logoutUser = (query = {}, token: null | undefined) => {
  return HttpClient.doGet('/logout', {}, token);
};
export const getSettings = () => {
  return HttpClient.doGet('/settings', {}, null);
};
export const getPromocode = (query = {}) => {
  return HttpClient.doGet('/cart/promocode/find', query, null);
};
