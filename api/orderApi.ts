import HttpClient from '../utils/httpClient';

export const getOrders = (query = {}, token = null) => {
  return HttpClient.doGet('/orders', query, token);
};
export const getOrderById = (id: any, token = null) => {
  return HttpClient.doGet('/order/' + id, {}, token);
};

export const checkout = (query = {}, token = null) => {
  return HttpClient.doPost('/checkout', query, token);
};
