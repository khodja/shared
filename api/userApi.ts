import HttpClient from '../utils/httpClient';

export const getUser = (query = {}, token = null) => {
  return HttpClient.doGet('/user', query, token);
};

export const updateUser = (data = {}, token = null) => {
  return HttpClient.doPut('/user/update', data, token);
};
