import HttpClient from '../utils/httpClient';

export const getCategories = (query: {} | undefined) => {
  return HttpClient.doGet('/categories', query);
};
