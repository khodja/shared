import HttpClient from "../utils/httpClient";

export const getFavourite = (query = {}, token = null) => {
  return HttpClient.doGet("/favorites", query, token);
};

export const addFavourite = (id: string, token = null) => {
  return HttpClient.doPut("/favorites/add/" + id, {}, token);
};

export const removeFavourite = (id: string, token = null) => {
  return HttpClient.doPut("/favorites/remove/" + id, {}, token);
};
