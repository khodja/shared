export const GET_ORDER_REQUEST = 'GET_ORDER_REQUEST';
export const GET_ORDER_SUCCESS = 'GET_ORDER_SUCCESS';
export const GET_ORDER_ERROR = 'GET_ORDER_ERROR';

export const SHOW_ORDER_REQUEST = 'SHOW_ORDER_REQUEST';
export const SHOW_ORDER_SUCCESS = 'SHOW_ORDER_SUCCESS';
export const SHOW_ORDER_ERROR = 'SHOW_ORDER_ERROR';

export const GET_ORDER_MORE_REQUEST = 'GET_ORDER_MORE_REQUEST';
export const GET_ORDER_MORE_SUCCESS = 'GET_ORDER_MORE_SUCCESS';
export const GET_ORDER_MORE_ERROR = 'GET_ORDER_MORE_ERROR';
