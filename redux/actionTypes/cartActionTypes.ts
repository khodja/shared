export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const CLEAR_CART = 'CLEAR_CART';
export const SET_ADDRESS = 'SET_ADDRESS';
export const CHANGE_DATA_CART = 'CHANGE_DATA_CART';
export const CART_UPDATE = 'CART_UPDATE';

export const STARTING_CART = 'STARTING_CART';
export const STOP_CART = 'STOP_CART';
export const SUCCESS_CART = 'SUCCESS_CART';
export const FAIL_CART = 'FAIL_CART';
