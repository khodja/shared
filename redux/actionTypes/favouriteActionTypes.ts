export const GET_FAVOURITE_REQUEST = "GET_FAVOURITE_REQUEST";
export const GET_FAVOURITE_SUCCESS = "GET_FAVOURITE_SUCCESS";
export const GET_FAVOURITE_ERROR = "GET_FAVOURITE_ERROR";

export const ADD_FAVOURITE_REQUEST = "ADD_FAVOURITE_REQUEST";
export const ADD_FAVOURITE_SUCCESS = "ADD_FAVOURITE_SUCCESS";
export const ADD_FAVOURITE_ERROR = "ADD_FAVOURITE_ERROR";

export const REMOVE_FAVOURITE_REQUEST = "REMOVE_FAVOURITE_REQUEST";
export const REMOVE_FAVOURITE_SUCCESS = "REMOVE_FAVOURITE_SUCCESS";
export const REMOVE_FAVOURITE_ERROR = "REMOVE_FAVOURITE_ERROR";