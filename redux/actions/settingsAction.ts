import { getSettings } from '../../api/authApi';
import * as TYPES from '../actionTypes/settingsActionTypes';

export const fetchSettings = () => (dispatch: any) => {
  return dispatch({
    api: getSettings,
    types: [
      TYPES.GET_SETTINGS_REQUEST,
      TYPES.GET_SETTINGS_SUCCESS,
      TYPES.GET_SETTINGS_ERROR,
    ],
    query: {},
  });
};
