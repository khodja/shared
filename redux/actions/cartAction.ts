import * as cartTypes from '../actionTypes/cartActionTypes';
import { checkout } from '../../api/orderApi';

export const changeProducts = (key: any, count: any) => {
  return {
    type: cartTypes.CHANGE_DATA_CART,
    payload: { key, count },
  };
};
export const cartUpdate = (key: any, count: number) => {
  return {
    type: cartTypes.CART_UPDATE,
    payload: { key, count },
  };
};
export const cartChange = (key: any, value: any) => {
  return {
    type: cartTypes.CHANGE_DATA_CART,
    payload: { key, value },
  };
};
export const cartEmpty = () => {
  return {
    type: cartTypes.SUCCESS_CART,
  };
};
export const cartChangeAddress = (data: {
  latitude: number;
  longitude: number;
  address: any;
}) => {
  return {
    type: cartTypes.SET_ADDRESS,
    payload: data,
  };
};
export const clearCart = () => {
  return {
    type: cartTypes.CLEAR_CART,
  };
};
export const cartAdd =
  ({ product, params, count }: any) =>
  (
    dispatch: (arg0: {
      type: string;
      payload: { product: any; params: any; count: any };
    }) => any,
  ) => {
    return dispatch({
      type: cartTypes.ADD_TO_CART,
      payload: { product, params, count },
    });
  };
export const cartRemove =
  (index: any) =>
  (dispatch: (arg0: { type: string; payload: any }) => any) => {
    return dispatch({
      type: cartTypes.REMOVE_FROM_CART,
      payload: index,
    });
  };
export const checkoutCart =
  (
    data: {
      promocode?: any;
      products?: any;
      shipping_type?: number;
      payment_type: any;
      lat?: any;
      long?: any;
      vippi_coins?: any;
    },
    callback = (id: any, url?: any): any => null,
    onError = (message?: string) => null,
  ) =>
  (dispatch: any) => {
    return dispatch({
      api: checkout,
      types: [
        cartTypes.STARTING_CART,
        data.payment_type === 0
          ? cartTypes.SUCCESS_CART
          : cartTypes.STOP_CART,
        cartTypes.FAIL_CART,
      ],
      query: data,
    })
      .then(
        (resp: {
          responseStatus: number;
          payload: { data: { id: any } };
        }) => {
          if (resp.responseStatus === 200) {
            if (resp.payload.data?.id) {
              callback(resp.payload.data.id);
            } else {
              callback(null, resp.payload.data);
            }
          }
        },
      )
      .catch(
        (error: {
          response: { data: { data: { message: string } } };
        }) => {
          onError();
        },
      );
  };
