import * as TYPES from '../actionTypes/compareActionTypes';

export const switchComparison = (payload: any) => {
  return {
    type: TYPES.COMPARE_SWITCH,
    payload,
  };
};
