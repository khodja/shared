import * as mainTypes from "../actionTypes/mainActionTypes";
import { getMain } from "../../api/mainApi";

export const getMainList = () => (dispatch: any) => {
  return dispatch({
    api: getMain,
    types: [
      mainTypes.GET_MAIN_REQUEST,
      mainTypes.GET_MAIN_SUCCESS,
      mainTypes.GET_MAIN_ERROR,
    ],
    query: {},
  });
};
