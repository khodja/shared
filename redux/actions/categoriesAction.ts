import * as categoriesActionTypes from '../actionTypes/categoriesActionTypes';
import { getCategories } from '../../api/categoriesApi';

export const getCategoriesList = () => (dispatch: any) => {
  return dispatch({
    api: getCategories,
    types: [
      categoriesActionTypes.GET_CATEGORIES_REQUEST,
      categoriesActionTypes.GET_CATEGORIES_SUCCESS,
      categoriesActionTypes.GET_CATEGORIES_ERROR,
    ],
    query: {},
  });
};
