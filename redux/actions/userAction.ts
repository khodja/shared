import * as TYPES from '../actionTypes/userActionTypes';
import { getUser, updateUser } from '../../api/userApi';
import { getPromocode } from '../../api/authApi';

export const getUserInfo = () => (dispatch: any) => {
  return dispatch({
    api: getUser,
    types: [
      TYPES.GET_USER_REQUEST,
      TYPES.GET_USER_SUCCESS,
      TYPES.GET_USER_ERROR,
    ],
    query: {},
  });
};

export const updateUserInfo =
  (data: any, callback?: () => void) => (dispatch: any) => {
    return dispatch({
      api: updateUser,
      types: [
        TYPES.UPDATE_USER_REQUEST,
        TYPES.UPDATE_USER_SUCCESS,
        TYPES.UPDATE_USER_ERROR,
      ],
      query: data,
    }).then((response: { responseStatus: number }) => {
      if (response.responseStatus === 200) {
        if (callback) callback();
        dispatch(getUserInfo());
      }
    });
  };

export const getPromocodeInfo = (name: any) => (dispatch: any) => {
  return dispatch({
    api: getPromocode,
    types: [
      TYPES.PROMOCODE_REQUEST,
      TYPES.PROMOCODE_SUCCESS,
      TYPES.PROMOCODE_ERROR,
    ],
    query: { name },
  });
};
export const clearPromocode = () => {
  return {
    type: TYPES.PROMOCODE_CLEAR,
  };
};
