import * as TYPES from '../actionTypes/viewedActionTypes';

export const addViewed = (product: any) => {
  return {
    type: TYPES.ADD_TO_VIEWED,
    payload: product,
  };
};
