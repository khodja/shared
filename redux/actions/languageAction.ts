export const availableLanguages = ["ru", "uz"];

export function switchLanguage(payload: any) {
  return {
    type: "SWITCH_LANGUAGE",
    payload,
  };
}
