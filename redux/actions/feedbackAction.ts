import * as feedbackActionTypes from '../actionTypes/feedbackActionTypes';
import {
  getFeedback,
  addFeedback,
  addFeedbackProduct,
} from '../../api/feedbackApi';

export const getFeedbackList =
  (id: number, page = 1) =>
  (dispatch: any) => {
    const isMore = page > 1 ? '_MORE' : '';
    return dispatch({
      api: getFeedback,
      types: [
        feedbackActionTypes[`GET${isMore}_FEEDBACK_REQUEST`],
        feedbackActionTypes[`GET${isMore}_FEEDBACK_SUCCESS`],
        feedbackActionTypes[`GET${isMore}_FEEDBACK_ERROR`],
      ],
      query: { id, page },
    });
  };

export const addOrderFeedback =
  (body: any, callback?: any) => (dispatch: any) => {
    return dispatch({
      api: addFeedback,
      types: [
        feedbackActionTypes.ADD_FEEDBACK_REQUEST,
        feedbackActionTypes.ADD_FEEDBACK_SUCCESS,
        feedbackActionTypes.ADD_FEEDBACK_ERROR,
      ],
      query: body,
    }).then(() => {
      if (callback) callback();
    });
  };
export const addProductFeedback =
  (body: any, callback?: any) => (dispatch: any) => {
    return dispatch({
      api: addFeedbackProduct,
      types: [
        feedbackActionTypes.ADD_FEEDBACK_REQUEST,
        feedbackActionTypes.ADD_FEEDBACK_SUCCESS,
        feedbackActionTypes.ADD_FEEDBACK_ERROR,
      ],
      query: body,
    }).then(() => {
      if (callback) callback();
    });
  };

export const clearFeedback = () => {
  return {
    type: feedbackActionTypes.CLEAR_FEEDBACK_ERROR,
  };
};
