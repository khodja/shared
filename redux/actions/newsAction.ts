import * as newsActionTypes from '../actionTypes/newsActionTypes';
import { getNews } from '../../api/newsApi';

export const getNewsList = () => (dispatch: any) => {
  return dispatch({
    api: getNews,
    types: [
      newsActionTypes.GET_NEWS_REQUEST,
      newsActionTypes.GET_NEWS_SUCCESS,
      newsActionTypes.GET_NEWS_ERROR,
    ],
    query: {},
  });
};
