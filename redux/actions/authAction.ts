import * as authActionTypes from '../actionTypes/authActionTypes';
import * as authApi from '../../api/authApi';

export const auth =
  (
    payload: { phone: string; code: string },
    callBack?: (status?: boolean) => void,
  ) =>
  (dispatch: any) => {
    const { phone, code: verify_code } = payload;
    dispatch({
      api: authApi.authUser,
      types: [
        authActionTypes.AUTH_GET_USER_TOKEN_REQUEST,
        authActionTypes.AUTH_GET_USER_TOKEN_SUCCESS,
        authActionTypes.AUTH_GET_USER_TOKEN_ERROR,
      ],
      query: {
        phone,
        verify_code,
      },
    })
      .then((response: { responseStatus: number }) => {
        if (callBack) callBack(response.responseStatus === 200);
      })
      .catch(() => {
        if (callBack) callBack(false);
      });
  };
export const verify =
  (
    payload: { phone: string },
    callBack?: (status?: boolean) => void,
  ) =>
  (dispatch: any) => {
    const { phone } = payload;

    dispatch({
      api: authApi.verifyUser,
      types: [
        authActionTypes.POST_PHONE_REQUEST,
        authActionTypes.POST_PHONE_SUCCESS,
        authActionTypes.POST_PHONE_ERROR,
      ],
      query: {
        phone,
      },
    })
      .then(async (response: { responseStatus: number }) => {
        if (callBack) callBack(response.responseStatus === 200);
      })
      .catch(() => {
        if (callBack) callBack(false);
      });
  };

export const logout = (callBack?: () => void) => (dispatch: any) => {
  dispatch({
    api: authApi.logoutUser,
    types: [authActionTypes.AUTH_LOGOUT, 'empty', 'empty'],
    query: {},
  }).then(async (response: { responseStatus: number }) => {
    if (response.responseStatus === 200) {
      if (callBack) callBack();
    }
  });
};

export function deleteToken() {
  return {
    type: authActionTypes.AUTH_LOGOUT,
  };
}
