import * as brandsActionTypes from '../actionTypes/brandsActionTypes';
import { getBrands, getProductsBrands } from '../../api/brandsApi';

export const getBrandsList = () => (dispatch: any) => {
  return dispatch({
    api: getBrands,
    types: [
      brandsActionTypes.GET_BRANDS_REQUEST,
      brandsActionTypes.GET_BRANDS_SUCCESS,
      brandsActionTypes.GET_BRANDS_ERROR,
    ],
    query: {},
  });
};

export const getProductsByBrand = (id: number) => (dispatch: any) => {
  return dispatch({
    api: getProductsBrands,
    types: [
      brandsActionTypes.GET_PRODUCTS_BY_BRANDS_REQUEST,
      brandsActionTypes.GET_PRODUCTS_BY_BRANDS_SUCCESS,
      brandsActionTypes.GET_PRODUCTS_BY_BRANDS_ERROR,
    ],
    query: id,
  });
};
