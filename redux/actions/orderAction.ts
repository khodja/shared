import * as orderActionTypes from '../actionTypes/orderActionTypes';
import { getOrderById, getOrders } from '../../api/orderApi';

export const getOrderList =
  (page = 1) =>
  (dispatch: any) => {
    const isMore = page > 1 ? '_MORE' : '';
    return dispatch({
      api: getOrders,
      types: [
        orderActionTypes[`GET_ORDER${isMore}_REQUEST`],
        orderActionTypes[`GET_ORDER${isMore}_SUCCESS`],
        orderActionTypes[`GET_ORDER${isMore}_ERROR`],
      ],
      query: { page },
    });
  };

export const getShowOrder = (id: any) => (dispatch: any) => {
  return dispatch({
    api: getOrderById,
    types: [
      orderActionTypes.SHOW_ORDER_REQUEST,
      orderActionTypes.SHOW_ORDER_SUCCESS,
      orderActionTypes.SHOW_ORDER_ERROR,
    ],
    query: id,
  });
};
