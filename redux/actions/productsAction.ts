import * as productsActionTypes from '../actionTypes/productsActionTypes';
import {
  getProducts,
  productById,
  searchProduct,
} from '../../api/productsApi';

export const getProductsList = () => (dispatch: any) => {
  return dispatch({
    api: getProducts,
    types: [
      productsActionTypes.GET_PRODUCTS_REQUEST,
      productsActionTypes.GET_PRODUCTS_SUCCESS,
      productsActionTypes.GET_PRODUCTS_ERROR,
    ],
    query: {},
  });
};

export const getProductById = (id: number) => (dispatch: any) => {
  return dispatch({
    api: productById,
    types: [
      productsActionTypes.GET_PRODUCT_REQUEST,
      productsActionTypes.GET_PRODUCT_SUCCESS,
      productsActionTypes.GET_PRODUCT_ERROR,
    ],
    query: id,
  });
};

export const searchProductList =
  ({ keyword, categories, brands, order_by, page = 1 }: any) =>
  (dispatch: any) => {
    const query = { keyword, categories, brands, order_by };
    const isMore = page > 1 ? '_MORE' : '';
    return dispatch({
      api: searchProduct,
      types: [
        productsActionTypes[`SEARCH${isMore}_REQUEST`],
        productsActionTypes[`SEARCH${isMore}_SUCCESS`],
        productsActionTypes.SEARCH_ERROR,
      ],
      query,
    });
  };
