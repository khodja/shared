import * as favouriteActionTypes from "../actionTypes/favouriteActionTypes";
import {
  getFavourite,
  addFavourite,
  removeFavourite,
} from "../../api/favouriteApi";

export const getFavouriteList = () => (dispatch: any) => {
  return dispatch({
    api: getFavourite,
    types: [
      favouriteActionTypes.GET_FAVOURITE_REQUEST,
      favouriteActionTypes.GET_FAVOURITE_SUCCESS,
      favouriteActionTypes.GET_FAVOURITE_ERROR,
    ],
    query: {},
  });
};

export const addToFavourite =
  (id: number, callback: any) => (dispatch: any) => {
    return dispatch({
      api: addFavourite,
      types: [
        favouriteActionTypes.ADD_FAVOURITE_REQUEST,
        favouriteActionTypes.ADD_FAVOURITE_SUCCESS,
        favouriteActionTypes.ADD_FAVOURITE_ERROR,
      ],
      query: id,
    }).then(() => {
      if (callback) callback();
    });
  };

export const removeFromFavourite =
  (id: number, callback: any) => (dispatch: any) => {
    return dispatch({
      api: removeFavourite,
      types: [
        favouriteActionTypes.REMOVE_FAVOURITE_REQUEST,
        favouriteActionTypes.REMOVE_FAVOURITE_SUCCESS,
        favouriteActionTypes.REMOVE_FAVOURITE_ERROR,
      ],
      query: id,
    }).then(() => {
      if (callback) callback();
    });
  };
