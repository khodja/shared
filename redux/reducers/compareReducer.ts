import * as TYPES from '../actionTypes/compareActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  products: [],
};

const reducers = {
  [TYPES.COMPARE_SWITCH](state: any, { payload }: any) {
    const { product, hasProduct } = payload;
    if (hasProduct) {
      state.products = state.products.filter(
        (item: any) => item.id !== product.id,
      );
    } else {
      state.products = [...state.products, product];
    }
  },
};

export default createReducer(initState, reducers);
