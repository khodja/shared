import * as types from '../actionTypes/cartActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  products: [],
  latitude: null,
  longitude: null,
  address: '',
  promocode: '',
  promocode_verified: false,
  shippingType: 1,
  loading: false,
};

const reducers = {
  [types.ADD_TO_CART](state: any, action: any) {
    state.products = [...state.products, action.payload];
  },
  [types.REMOVE_FROM_CART](state = initState, action: any) {
    state.products = state.products.filter(
      (_, index: number) => index !== action.payload,
    );
  },
  [types.CLEAR_CART](state: any) {
    state = initState;
  },
  [types.SET_ADDRESS](state: any, { payload }: any) {
    const { latitude, longitude, address } = payload;
    state.latitude = latitude;
    state.longitude = longitude;
    state.address = address;
  },
  [types.CHANGE_DATA_CART](state: any, { payload }: any) {
    const { key, value } = payload;
    state[key] = value;
  },
  [types.CART_UPDATE](state: any, { payload }: any) {
    const { count, key } = payload;
    const mutated = state.products;
    mutated[key].count = count;
    state.products = [...mutated];
  },
  [types.STARTING_CART](state: any) {
    state.loading = true;
  },
  [types.SUCCESS_CART](state: any) {
    state.products = [];
    state.lat = null;
    state.long = null;
    state.address = '';
    state.promocode = '';
    state.promocode_verified = false;
    state.shippingType = 1;
    state.loading = false;
  },
  [types.FAIL_CART](state: any) {
    state.loading = false;
  },
  [types.STOP_CART](state: any) {
    state.loading = false;
  },
};

export default createReducer(initState, reducers);
