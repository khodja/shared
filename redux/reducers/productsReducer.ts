import * as productsActionTypes from '../actionTypes/productsActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  products: [],
  loading: false,
  error: null,
};

const reducers = {
  [productsActionTypes.GET_PRODUCTS_REQUEST](state: {
    loading: boolean;
    error: null;
  }) {
    state.loading = true;
    state.error = null;
  },
  [productsActionTypes.GET_PRODUCTS_SUCCESS](
    state: { products: any; loading: boolean },
    action: { payload: { data: any } },
  ) {
    state.products = action.payload.data;
    state.loading = false;
  },
  [productsActionTypes.GET_PRODUCTS_ERROR](
    state: { loading: boolean; error: any; products: never[] },
    action: { error: any },
  ) {
    state.loading = false;
    state.error = action.error;
    state.products = [];
  },
};

export default createReducer(initState, reducers);
