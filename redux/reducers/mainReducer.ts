import * as mainTypes from '../actionTypes/mainActionTypes';
import { createReducer } from '../../utils/storeUtils';

export interface MainState {
  data: Array<any>;
  loading: boolean;
  error: {
    message: string;
    code: number;
  } | null;
}
const initState: MainState = {
  data: [],
  loading: false,
  error: null,
};

const reducers = {
  [mainTypes.GET_MAIN_REQUEST](state: {
    loading: boolean;
    error: null;
  }) {
    state.loading = true;
    state.error = null;
  },
  [mainTypes.GET_MAIN_SUCCESS](
    state: { main: any; loading: boolean },
    action: { payload: { data: any } },
  ) {
    state.main = action.payload.data;
    state.loading = false;
  },
  [mainTypes.GET_MAIN_ERROR](
    state: { loading: boolean; error: any; main: never[] },
    action: { payload: { error: any } },
  ) {
    state.loading = false;
    state.error = action.payload.error;
    state.main = [];
  },
};

export default createReducer(initState, reducers);
