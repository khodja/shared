import * as feedbackActionTypes from '../actionTypes/feedbackActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  feedbacks: [],
  moreFeedbacks: [],
  loading: false,
  error: null,
  moreLoading: false,
  hasMore: false,
};

const reducers = {
  [feedbackActionTypes.GET_FEEDBACK_REQUEST](state: any) {
    state.feedbacks = [];
    state.loading = true;
    state.error = null;
    state.hasMore = false;
  },
  [feedbackActionTypes.GET_FEEDBACK_SUCCESS](
    state: any,
    action: any,
  ) {
    state.feedbacks = action.payload.data;
    state.moreFeedbacks = action.payload.data;
    state.loading = false;
    state.hasMore = !!action.payload.next_page_url;
  },
  [feedbackActionTypes.GET_FEEDBACK_ERROR](state: any, action: any) {
    state.loading = false;
    state.error = action.error;
    state.feedbacks = [];
    state.moreFeedbacks = [];
    state.hasMore = false;
  },
  [feedbackActionTypes.GET_MORE_FEEDBACK_REQUEST](state: any) {
    state.moreLoading = true;
    state.error = null;
    state.hasMore = false;
  },
  [feedbackActionTypes.GET_MORE_FEEDBACK_SUCCESS](
    state: any,
    action: any,
  ) {
    state.moreLoading = false;
    state.moreFeedbacks = [
      ...state.moreFeedbacks,
      ...action.payload.data,
    ];
    state.error = null;
    state.hasMore = !!action.payload.next_page_url;
  },
  [feedbackActionTypes.GET_MORE_FEEDBACK_ERROR](
    state: any,
    action: any,
  ) {
    state.moreLoading = false;
    state.error = action.error;
  },
  [feedbackActionTypes.ADD_FEEDBACK_REQUEST](state: any) {
    state.loading = true;
    state.error = null;
  },
  [feedbackActionTypes.ADD_FEEDBACK_SUCCESS](
    state: any,
    action: any,
  ) {
    state.loading = false;
  },
  [feedbackActionTypes.ADD_FEEDBACK_ERROR](state: any, action: any) {
    state.loading = false;
    state.error = action.error;
  },
  [feedbackActionTypes.CLEAR_FEEDBACK_ERROR](state: any) {
    state.loading = false;
    state.error = null;
  },
};

export default createReducer(initState, reducers);
