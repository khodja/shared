import * as productsActionTypes from '../actionTypes/brandsActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  loading: false,
  error: null,
  products: [],
};

const reducers = {
  [productsActionTypes.GET_PRODUCTS_BY_BRANDS_REQUEST](
    state = initState,
  ) {
    state.loading = true;
    state.error = null;
  },
  [productsActionTypes.GET_PRODUCTS_BY_BRANDS_SUCCESS](
    state = initState,
    action: any,
  ) {
    state.products = action.payload.data;
    state.loading = false;
  },
  [productsActionTypes.GET_PRODUCTS_BY_BRANDS_ERROR](
    state = initState,
    action: any,
  ) {
    state.loading = false;
    state.error = action.error;
    state.products = [];
  },
};

export default createReducer(initState, reducers);
