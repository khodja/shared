import * as brandsActionTypes from '../actionTypes/brandsActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  brands: [],
  loading: true,
  error: null,
};

const reducers = {
  [brandsActionTypes.GET_BRANDS_REQUEST](state = initState) {
    state.loading = true;
    state.error = null;
  },
  [brandsActionTypes.GET_BRANDS_SUCCESS](
    state = initState,
    action: any,
  ) {
    state.brands = action.payload.data;
    state.loading = false;
    state.error = null;
  },
  [brandsActionTypes.GET_BRANDS_ERROR](
    state = initState,
    action: any,
  ) {
    state.loading = false;
    state.error = action.error;
    state.brands = [];
  },
};

export default createReducer(initState, reducers);
