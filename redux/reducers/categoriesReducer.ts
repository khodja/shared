import * as types from '../actionTypes/categoriesActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  categories: [],
  mainCategories: [],
  loading: false,
  error: null,
};

const reducers = {
  [types.GET_CATEGORIES_REQUEST](state: any) {
    state.loading = true;
    state.error = null;
  },
  [types.GET_CATEGORIES_SUCCESS](state: any, action: any) {
    state.categories = action.payload.data;
    state.mainCategories = action.payload.data.filter(
      (cat: any) => !cat.parent_id,
    );
    state.loading = false;
  },
  [types.GET_CATEGORIES_ERROR](state: any, action: any) {
    state.loading = false;
    state.error = action.error;
    state.categories = [];
  },
};

export default createReducer(initState, reducers);
