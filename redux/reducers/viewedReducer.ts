import * as TYPES from '../actionTypes/viewedActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  viewed: [],
};

const reducers = {
  [TYPES.ADD_TO_VIEWED](
    state: { viewed: any[] },
    action: { payload: { id: any } },
  ) {
    const data = [];
    let hasProduct = false;
    state.viewed.some((product: { id: any }, index: number) => {
      if (action.payload.id === product.id) {
        hasProduct = true;
        data.push(action.payload);
        return false;
      }
      if (index === 9) return true;
      data.push(product);
      return false;
    });
    if (!hasProduct) data.push(action.payload);

    state.viewed = data;
  },
};

export default createReducer(initState, reducers);
