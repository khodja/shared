import * as TYPES from '../actionTypes/settingsActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  data: {},
  loading: true,
  error: null,
};

const reducers = {
  [TYPES.GET_SETTINGS_REQUEST](state: {
    loading: boolean;
    error: null;
    data: {};
  }) {
    state.loading = true;
    state.error = null;
    state.data = {};
  },
  [TYPES.GET_SETTINGS_SUCCESS](
    state: { data: any; loading: boolean; error: null },
    action: { payload: { data: any } },
  ) {
    state.data = action.payload.data;
    state.loading = false;
    state.error = null;
  },
  [TYPES.GET_SETTINGS_ERROR](
    state: { loading: boolean; error: any; data: {} },
    action: { error: any },
  ) {
    state.loading = false;
    state.error = action.error;
    state.data = {};
  },
};

export default createReducer(initState, reducers);
