import { combineReducers } from 'redux';
import main from './mainReducer';
import language from './languageReducer';
import categories from './categoriesReducer';
import news from './newsReducer';
import brands from './brandsReducer';
import products from './productsReducer';
import product from './productReducer';
import brandProduct from './brandProductReducer';
import orders from './ordersReducer';
import order from './showOrderReducer';
import favourite from './favouriteReducer';
import feedback from './feedbackReducer';
import auth from './authReducer';
import cart from './cartReducer';
import user from './userReducer';
import search from './searchReducer';
import settings from './settingsReducer';
import promocode from './promocodeReducer';
import viewed from './viewedReducer';
import compare from './compareReducer';

export const rootReducer = combineReducers({
  main,
  language,
  categories,
  brands,
  news,
  products,
  orders,
  favourite,
  feedback,
  product,
  brandProduct,
  auth,
  user,
  order,
  cart,
  search,
  settings,
  promocode,
  viewed,
  compare,
});
