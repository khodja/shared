import * as TYPES from '../actionTypes/userActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  data: {},
  loading: false,
  error: null,
};

const reducers = {
  [TYPES.PROMOCODE_REQUEST](state = initState) {
    state.loading = true;
    state.error = null;
    state.data = {};
  },
  [TYPES.PROMOCODE_SUCCESS](
    state = initState,
    action: { payload: { data: {} } },
  ) {
    state.data = action.payload.data;
    state.loading = false;
    state.error = null;
  },
  [TYPES.PROMOCODE_ERROR](state = initState, action: { error: any }) {
    state.loading = false;
    state.error = action.error;
    state.data = {};
  },
  ['SUCCESS_CART'](state = initState) {
    state.loading = false;
    state.error = null;
    state.data = {};
  },
  ['CLEAR_CART'](state = initState) {
    state.loading = false;
    state.error = null;
    state.data = {};
  },
  [TYPES.PROMOCODE_CLEAR](state = initState) {
    state.loading = false;
    state.error = null;
    state.data = {};
  },
};

export default createReducer(initState, reducers);
