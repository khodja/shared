const initialState = {
  language: "ru",
};

function languageReducer(state = initialState, { type, payload }: any) {
  switch (type) {
    case "SWITCH_LANGUAGE":
      return { ...state, language: payload };
    default:
      return state;
  }
}
export default languageReducer;
