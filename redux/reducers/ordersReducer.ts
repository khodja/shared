import * as orderActionTypes from '../actionTypes/orderActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  loading: false,
  orders: [],
  error: null,
  hasMore: false,
  moreLoading: false,
};

const reducers = {
  [orderActionTypes.GET_ORDER_REQUEST](state: any) {
    state.loading = true;
    state.error = null;
    state.hasMore = false;
    state.orders = [];
  },
  [orderActionTypes.GET_ORDER_SUCCESS](state: any, action: any) {
    state.loading = false;
    state.orders = action.payload.data;
    state.hasMore = !!action.payload.next_page_url;
    state.error = null;
  },
  [orderActionTypes.GET_ORDER_ERROR](state: any, action: any) {
    state.loading = false;
    state.error = action.error;
    state.orders = [];
    state.hasMore = false;
  },
  [orderActionTypes.GET_ORDER_MORE_REQUEST](state: any) {
    state.moreLoading = true;
    state.error = null;
    state.hasMore = false;
  },
  [orderActionTypes.GET_ORDER_MORE_SUCCESS](state: any, action: any) {
    state.moreLoading = false;
    state.orders = [...state.orders, ...action.payload.data];
    state.error = null;
    state.hasMore = !!action.payload.next_page_url;
  },
  [orderActionTypes.GET_ORDER_MORE_ERROR](state: any, action: any) {
    state.moreLoading = false;
    state.error = action.error;
    state.orders = [];
    state.hasMore = false;
  },
};

export default createReducer(initState, reducers);
