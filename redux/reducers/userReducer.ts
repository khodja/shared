import * as TYPES from '../actionTypes/userActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  user: {},
  loading: true,
  error: null,
};

const reducers = {
  [TYPES.GET_USER_REQUEST](state: { loading: boolean; error: null }) {
    state.loading = true;
    state.error = null;
  },
  [TYPES.GET_USER_SUCCESS](
    state: { user: any; loading: boolean; error: null },
    action: { payload: { data: any } },
  ) {
    state.user = action.payload.data;
    state.loading = false;
    state.error = null;
  },
  [TYPES.GET_USER_ERROR](
    state: { loading: boolean; error: any; user: {} },
    action: { error: any },
  ) {
    state.loading = false;
    state.error = action.error;
    state.user = {};
  },
  [TYPES.CHANGE_USER](
    state: { loading: boolean; error: any; user: any },
    action: { error: any; payload: any },
  ) {
    state.loading = false;
    state.error = action.error;
    state.user = { ...state.user, ...action.payload };
  },
  [TYPES.UPDATE_USER_REQUEST](state: {
    loading: boolean;
    error: null;
  }) {
    state.loading = true;
    state.error = null;
  },
  [TYPES.UPDATE_USER_SUCCESS](
    state: { loading: boolean; error: null },
    action: any,
  ) {
    state.loading = false;
    state.error = null;
  },
  [TYPES.UPDATE_USER_ERROR](
    state: { loading: boolean; error: any },
    action: { error: any },
  ) {
    state.loading = false;
    state.error = action.error;
  },
  [TYPES.UPDATE_USER_ERROR](
    state: { loading: boolean; error: any },
    action: { error: any },
  ) {
    state.loading = false;
    state.error = action.error;
  },
  ['AUTH_LOGOUT'](state: { user: {}; error: null }) {
    state.user = {};
    state.error = null;
  },
};

export default createReducer(initState, reducers);
