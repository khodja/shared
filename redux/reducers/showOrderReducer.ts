import * as orderActionTypes from '../actionTypes/orderActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  loading: true,
  order: {},
  error: null,
};

const reducers = {
  [orderActionTypes.SHOW_ORDER_REQUEST](state: {
    loading: boolean;
    error: null;
  }) {
    state.loading = true;
    state.error = null;
  },
  [orderActionTypes.SHOW_ORDER_SUCCESS](
    state: { loading: boolean; order: any; error: null },
    action: { payload: { data: any } },
  ) {
    state.loading = false;
    state.order = action.payload.data;
    state.error = null;
  },
  [orderActionTypes.SHOW_ORDER_ERROR](
    state: { loading: boolean; error: any; order: {} },
    action: { error: any },
  ) {
    state.loading = false;
    state.error = action.error;
    state.order = {};
  },
};

export default createReducer(initState, reducers);
