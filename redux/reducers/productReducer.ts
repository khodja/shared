import * as productsActionTypes from '../actionTypes/productsActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  loading: true,
  error: null,
  product: {},
};

const reducers = {
  [productsActionTypes.GET_PRODUCT_REQUEST](state: {
    loading: boolean;
    error: null;
  }) {
    state.loading = true;
    state.error = null;
  },
  [productsActionTypes.GET_PRODUCT_SUCCESS](
    state: { product: any; loading: boolean; error: null },
    action: { payload: { data: any } },
  ) {
    state.product = action.payload.data;
    state.loading = false;
    state.error = null;
  },
  [productsActionTypes.GET_PRODUCT_ERROR](
    state: { loading: boolean; error: any; product: {} },
    action: { error: any },
  ) {
    state.loading = false;
    state.error = action.error;
    state.product = {};
  },
};

export default createReducer(initState, reducers);
