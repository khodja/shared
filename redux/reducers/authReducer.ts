import * as authActionTypes from '../actionTypes/authActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  token: null,
  loading: false,
  error: null,
};

const reducers = {
  [authActionTypes.AUTH_GET_USER_TOKEN_REQUEST](state = initState) {
    state.loading = true;
    state.error = null;
  },
  [authActionTypes.AUTH_GET_USER_TOKEN_SUCCESS](
    state = initState,
    action: any,
  ) {
    state.token = action.payload.data.token;
    state.loading = false;
    state.error = null;
  },
  [authActionTypes.AUTH_LOGOUT](state = initState) {
    state.token = null;
    state.loading = false;
    state.error = null;
  },
  [authActionTypes.AUTH_GET_USER_TOKEN_ERROR](
    state = initState,
    action: any,
  ) {
    state.loading = false;
    state.error = action.error;
    state.token = null;
  },
};

export default createReducer(initState, reducers);
