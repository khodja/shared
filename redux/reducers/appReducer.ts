import * as authActionTypes from "../actionTypes/authActionTypes";
import { createReducer } from "../../utils/storeUtils";

const initState = {
  token: null,
  language: "ru",
};

const reducers = {
  [authActionTypes.AUTH_SAVE_TOKEN](
    state = initState,
    action: { payload: { accessToken: null } }
  ) {
    state.token = action.payload.accessToken;
  },
  [authActionTypes.AUTH_LOGOUT](state = initState) {
    state.token = null;
  },
  ["SWITCH_LANGUAGE"](state = initState, action: any) {
    state.language = action.payload;
  },
};

export default createReducer(initState, reducers);
