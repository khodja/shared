import * as newsActionTypes from '../actionTypes/newsActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  news: [],
  promos: [],
  slider: [],
  loading: false,
  error: null,
};

const reducers = {
  [newsActionTypes.GET_NEWS_REQUEST](state: any) {
    state.loading = true;
    state.error = null;
  },
  [newsActionTypes.GET_NEWS_SUCCESS](state: any, action: any) {
    const news: any = [];
    const promos: any = [];
    const slider: any = [];
    action.payload.data.forEach((item: any) => {
      if (Boolean(item.promo)) promos.push(item);
      if (Boolean(!item.promo)) news.push(item);
      if (Boolean(item.slider)) slider.push(item);
    });
    state.news = news;
    state.promos = promos;
    state.slider = slider;
    state.loading = false;
  },
  [newsActionTypes.GET_NEWS_ERROR](state: any, action: any) {
    state.loading = false;
    state.error = action.error;
    state.news = [];
    state.slider = [];
    state.promos = [];
  },
};

export default createReducer(initState, reducers);
