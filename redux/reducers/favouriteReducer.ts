import * as favouriteActionTypes from '../actionTypes/favouriteActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  loading: true,
  error: null,
  products: [],
  switchLoading: false,
};

const reducers = {
  [favouriteActionTypes.GET_FAVOURITE_REQUEST](state = initState) {
    state.loading = true;
    state.error = null;
  },
  [favouriteActionTypes.GET_FAVOURITE_SUCCESS](
    state = initState,
    action: { payload: { data: any } },
  ) {
    state.products = action.payload.data;
    state.loading = false;
    state.error = null;
  },
  [favouriteActionTypes.GET_FAVOURITE_ERROR](
    state = initState,
    action: { error: null },
  ) {
    state.loading = false;
    state.error = action.error;
    state.products = [];
  },
  [favouriteActionTypes.ADD_FAVOURITE_REQUEST](state = initState) {
    state.switchLoading = true;
    state.error = null;
  },
  [favouriteActionTypes.ADD_FAVOURITE_SUCCESS](
    state = initState,
    action: any,
  ) {
    state.switchLoading = false;
    state.error = null;
  },
  [favouriteActionTypes.ADD_FAVOURITE_ERROR](
    state = initState,
    action: { error: null },
  ) {
    state.switchLoading = false;
    state.error = action.error;
  },
  [favouriteActionTypes.REMOVE_FAVOURITE_REQUEST](state = initState) {
    state.switchLoading = true;
    state.error = null;
  },
  [favouriteActionTypes.REMOVE_FAVOURITE_SUCCESS](
    state = initState,
    action: any,
  ) {
    state.switchLoading = false;
    state.error = null;
  },
  [favouriteActionTypes.REMOVE_FAVOURITE_ERROR](
    state = initState,
    action: { error: null },
  ) {
    state.switchLoading = false;
    state.error = action.error;
  },
};

export default createReducer(initState, reducers);
