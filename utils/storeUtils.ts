import createNextState from 'immer';

export const getInitialState = (initState: any) => initState;

export function createReducer(initialState: any, actionsMap: any) {
  return function (state = initialState, action: any) {
    return createNextState(state, (draft: any) => {
      const caseReducer = actionsMap[action.type];
      if (caseReducer) {
        return caseReducer(draft, action);
      }
      return draft;
    });
  };
}
